package com.firmanpro.moviepro;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.firmanpro.moviepro.adapter.MovieAdapter;
import com.firmanpro.moviepro.entity.MovieEntity;
import com.firmanpro.moviepro.helper.MovieHelper;
import com.firmanpro.moviepro.service.MovieService;

import java.util.ArrayList;
import java.util.List;

public class MovieActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = "main_act";
    private MovieService mainService;
    private SwipeRefreshLayout swipe_refresh;
    public static int currentPage = 1;
    private RecyclerView rv_movie;
    private RecyclerView.LayoutManager lm_movie;
    private MovieAdapter movieAdapter;
    private List<MovieEntity.ResultsBean> movieList = new ArrayList<MovieEntity.ResultsBean>();
    private int columnItem = 1;
    private GridLayoutManager layoutManager;
    private Spinner spin_category;
    private ArrayAdapter<String> movieCategoryAdapter;
    private int positionCategory = 0;
    private CardView cv_search;

    public static boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);


        /**inisialisasi Fresco image view*/
        Fresco.initialize(this);

        /**inisialisasi service*/
        mainService = new MovieService();

        /**inisialisasi swipe*/
        swipe_refresh = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        /**untuk menjalankan onRefresh()*/
        swipe_refresh.setOnRefreshListener(this);

        /**inisialisasi spinner movie category*/
        spin_category = (Spinner)findViewById(R.id.spin_category);
        movieCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_movie_category, MovieHelper.movieCategory());
        spin_category.setAdapter(movieCategoryAdapter);

        /**inisialisasi RecyclerView*/
        rv_movie = (RecyclerView)findViewById(R.id.rv_movie);
        lm_movie = new GridLayoutManager(this, columnItem); /**kolom tampilan item*/
        rv_movie.setLayoutManager(lm_movie);
        rv_movie.setItemAnimator(new DefaultItemAnimator());
        movieAdapter = new MovieAdapter(getApplicationContext(), movieList);
        rv_movie.setAdapter(movieAdapter);

        /**inisialisasi Cardview Search*/
        cv_search = (CardView)findViewById(R.id.cv_search);

    }

    @Override
    protected void onStart() {
        super.onStart();

        /**secara default spin_category akan lebih dulu dijalankan
         * jika cv_search is VISIBLE*/
        if (cv_search.getVisibility()==View.GONE){
            currentPage = 1;
            positionCategory = 0;
            loadDataMovie();
        }

        /**load data movie berdasarkan category yang dipilih. defaultnya category now playing*/
        spin_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                movieList.clear();
                positionCategory = position;
                loadDataMovie();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /**Pagination when scroll down*/
        rv_movie.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                /**check for scroll down*/
                if (dy > 0) {
                    totalItemCount = lm_movie.getItemCount(); /**total semua data item yang dipanggil dari server secara keseluruhan*/
                    visibleItemCount = lm_movie.getChildCount(); /**total item yang muncul sekarang (current show)*/
                    layoutManager = ((GridLayoutManager) rv_movie.getLayoutManager());
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition(); /**total item yang muncul sebelumnya*/
                    /**if loading true :
                     * sebagai penanda, paging boleh dilakukan kembali jika current scroll paging done (load data selesai)*/
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            currentPage += 1;
                            mainService.loadNowPlaying(getApplicationContext(),
                                    currentPage, swipe_refresh, movieList, movieAdapter, positionCategory);
                        }
                    }
                }
            }
        });


    }

    /**load data movie berdasarkan movie category*/
    private void loadDataMovie() {
        swipe_refresh.post(new Runnable() {
               @Override
               public void run() {
                   mainService.loadNowPlaying(getApplicationContext(), currentPage, swipe_refresh, movieList, movieAdapter, positionCategory);
               }
           }
        );
    }

    @Override
    public void onRefresh() {
        /**aksi ini saat activity ini diswipe down/pull*/
        movieList.clear();
        currentPage = 1;
        mainService.loadNowPlaying(getApplicationContext(), currentPage, swipe_refresh, movieList, movieAdapter, positionCategory);
    }

    /**inisilasisasi icon menu yang muncul pada action bar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movie, menu);
        return true;
    }

    /**set menu action*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_search:
                if (cv_search.getVisibility()==View.VISIBLE)
                    cv_search.setVisibility(View.GONE);
                else
                    cv_search.setVisibility(View.VISIBLE);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
