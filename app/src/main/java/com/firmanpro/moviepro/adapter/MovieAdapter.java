package com.firmanpro.moviepro.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.firmanpro.moviepro.R;
import com.firmanpro.moviepro.config.ApiLink;
import com.firmanpro.moviepro.entity.MovieEntity;
import com.firmanpro.moviepro.helper.MovieHelper;

import java.util.List;

/**
 * Created by firmanmac on 10/7/17.
 *
 * Urutan mudah membuat adapter:
 *  1. MyViewHolder
 *  2. extend RecyclerView.Adapter<MovieAdapter.MyViewHolder>
 *  3. implement method
 *  4. private Context context;
 *  5. List<MovieEntity.ResultsBean> resultsBeanList;
 *  6. buat constractor
 *  7. lengkapi onCreateViewHolder
 *  8. lengkapi getItemCount
 *  9. lengkapi onBindViewHolder
 */


public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    protected Context context;
    protected List<MovieEntity.ResultsBean> resultsBeanList;
    protected Uri imageUri;
    protected MovieEntity.ResultsBean data;

    public MovieAdapter(Context context, List<MovieEntity.ResultsBean> resultsBeanList) {
        this.context = context;
        this.resultsBeanList = resultsBeanList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        data = resultsBeanList.get(position);
        holder.tv_original_title.setText(data.getOriginal_title());
        holder.tv_release_date.setText(MovieHelper.beauty_date(data.getRelease_date()));
        holder.tv_vote_average.setText(String.valueOf(data.getVote_average()));
        holder.tv_overview.setText(data.getOverview());
        imageUri = Uri.parse(ApiLink.POSTER + data.getPoster_path());
        holder.iv_poster_path.setImageURI(imageUri);
    }

    @Override
    public int getItemCount() {
        return resultsBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        protected TextView tv_original_title, tv_release_date, tv_vote_average, tv_overview;
        protected SimpleDraweeView iv_poster_path;

        public MyViewHolder (View itemView) {
            super(itemView);
            tv_original_title = (TextView)itemView.findViewById(R.id.tv_original_title);
            tv_release_date = (TextView)itemView.findViewById(R.id.tv_release_date);
            tv_vote_average = (TextView)itemView.findViewById(R.id.tv_vote_average);
            tv_overview = (TextView)itemView.findViewById(R.id.tv_overview);
            iv_poster_path = (SimpleDraweeView)itemView.findViewById(R.id.iv_poster_path);
        }
    }

}
