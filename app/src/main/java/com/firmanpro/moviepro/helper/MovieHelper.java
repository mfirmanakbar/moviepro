package com.firmanpro.moviepro.helper;

import com.firmanpro.moviepro.config.ApiLink;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by firmanmac on 10/8/17.
 */

public class MovieHelper {

    protected static SimpleDateFormat spf = null;
    protected static List<String> movieCategoryList = new ArrayList<String>();
    protected static Date newDate = null;

    public static String beauty_date(String strCurrentDate){
        spf = new SimpleDateFormat("yyyy-MM-dd");
        newDate = null;
        try {
            newDate = spf.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("dd MMM yyyy");
        return spf.format(newDate);
    }

    public static List<String> movieCategory(){
        movieCategoryList.clear();
        movieCategoryList.add("Now Playing");
        movieCategoryList.add("Upcoming");
        movieCategoryList.add("Top Rated");
        movieCategoryList.add("Popular");
        return movieCategoryList;
    }

    public static String URL_MOVIE(int categoryPosition){
        if (categoryPosition == 0)
            return ApiLink.NOW_PLAYING;
        else if (categoryPosition == 1)
            return ApiLink.UPCOMING;
        else if (categoryPosition == 2)
            return ApiLink.TOP_RATED;
        else
            return ApiLink.POPULAR;
    }

}
