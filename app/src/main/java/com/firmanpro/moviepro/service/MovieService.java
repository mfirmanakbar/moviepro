package com.firmanpro.moviepro.service;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firmanpro.moviepro.MovieActivity;
import com.firmanpro.moviepro.adapter.MovieAdapter;
import com.firmanpro.moviepro.entity.MovieEntity;
import com.firmanpro.moviepro.helper.MovieHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by firmanmac on 10/7/17.
 */

public class MovieService {

    private final String TAG = "main_service";
    private JsonObjectRequest jsonObjectRequest;
    private Gson gsonRes;
    private String finalUrlAPI;
    private MovieEntity movieEntity;

    /**
     * fungsi panggil data movie
     * */
    public void loadNowPlaying(final Context context, final int page, final SwipeRefreshLayout swipe_refresh,
                               final List<MovieEntity.ResultsBean> nowPlayingList, final MovieAdapter movieAdapters, int positionCategory){

        /**loading show*/
        swipe_refresh.setRefreshing(true);

        /**set link URL API final*/
        finalUrlAPI = MovieHelper.URL_MOVIE(positionCategory) + String.valueOf(page);
        Log.d(TAG, "URL API : " + finalUrlAPI);

        /**inisialisasi dan set parameter*/
        jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, /**Method GET & POST*/
                finalUrlAPI, /**URL API*/
                //"", /**Opsinal : isi dari data/Body yang akan dikirimkan dengan bentuk JSON */
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        /**respon dari API*/

                        Log.d(TAG, "Response - Begin");
                        Log.d(TAG, response.toString());
                        Log.d(TAG, "Response - End");

                        gsonRes = new GsonBuilder().create();
                        movieEntity = gsonRes.fromJson(response.toString(), MovieEntity.class);

                        /** adding data moview to list*/
                        nowPlayingList.addAll(movieEntity.getResults());
                        movieAdapters.notifyDataSetChanged();

                        /** update currentPage */
                        MovieActivity.currentPage = page;
                        //Toast.makeText(context,"Page "+String.valueOf(MovieActivity.currentPage),Toast.LENGTH_SHORT).show();

                        /** after finish get data*/
                        MovieActivity.loading = true;

                        /**loading close*/
                        swipe_refresh.setRefreshing(false);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /**respon error*/
                Log.e(TAG, error.getMessage());
                MovieActivity.loading = true;
                /**loading close*/
                swipe_refresh.setRefreshing(false);
            }
        });

        /**
         * Opsinal : Config request custome - sifatnya Opsional
         * ------------------------------------------------------------------
         * Contoh :
         * custome timeout
         * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS = 25 detik/2500 milidetik
         * bisa diganti sesuai kebutuhan contoh 50 detik
         * ------------------------------------------------------------------
         * */
        /*jsonObjectRequest.setRetryPolicy(
                new DefaultRetryPolicy(
                    //DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT //before custome
                    5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT //after custome
                )
        );*/

        /**request*/
        Volley.newRequestQueue(context).add(jsonObjectRequest);
    }

}
