package com.firmanpro.moviepro.config;

/**
 * Created by firmanmac on 10/7/17.
 */

public class ApiLink {

    /**key api terdaftar dari themoviedb*/
    public final static String API_KEY = "88e7b3c8f61930554dcabfca599bdc73";

    /**link API untuk memunculkan data movie */
    public static String NOW_PLAYING = "https://api.themoviedb.org/3/movie/now_playing?api_key=" + API_KEY +"&language=en-US&page=";
    public static String UPCOMING = "https://api.themoviedb.org/3/movie/upcoming?api_key=" + API_KEY +"&language=en-US&page=";
    public static String TOP_RATED = "https://api.themoviedb.org/3/movie/top_rated?api_key=" + API_KEY +"&language=en-US&page=";
    public static String POPULAR = "https://api.themoviedb.org/3/movie/popular?api_key=" + API_KEY +"&language=en-US&page=";

    /**link API untuk memunculkan poster movie */
    public static String POSTER = "https://image.tmdb.org/t/p/w154";

}
